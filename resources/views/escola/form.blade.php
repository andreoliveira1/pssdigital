@extends('layout.principal')
@section('titulo')

@stop
@section('conteudo')
<h1>Cadastrar Escola</h1>
    <form class="" action="/escola/create" method="post">
        {{ csrf_field() }}
        @if(session('erro'))
            <div class="alert alert-danger">
              {{  session('erro') }}
            </div>
        @endif
				@if(session('sucesso'))
            <div class="alert alert-success">
              {{  session('sucesso') }}
            </div>
        @endif
    <div class="form-group">
      <label for="nome">Nome: </label>
      <input type="text" style="width:500px" name="nome" value="" class="form-control" placeholder="Inserir o nome da escola" id="nome" required name="nome">
    </div>
    <div class="form-group">
      <label for="nucleo">Núcleo: </label>
      <select style="width:150px" name="nucleo" id="nucleo" class="selectpicker show-tick form-control" required name="nucleo">
        <option value="">Escolher Núcleo</option>
        @foreach ($nucleos as $kNucleo => $vNucleo)
          <option value="{{$vNucleo->id}}">{{$vNucleo->nome}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label>Endereço:</label>
    </div>
    <div class="form-group">
      <label for="cep">CEP: </label>
      <input type="text" style="width:150px" name="cep" class="form-control" value="" placeholder="00000000" id="cep" required name="cep">
    </div>
    <div class="form-group">
      <label for="rua">Rua: </label>
      <input type="text" style="width:500px" name="rua" value="" class="form-control" placeholder="Nome da rua" id="rua" required name="rua">
    </div>
    <div class="form-group">
      <label for="numero">Numero: </label>
      <input type="text" style="width:150px" name="numero" value="" class="form-control" placeholder="00000" id="numero" required name="numero">
    </div>
    <div class="form-group">
      <label for="bairro">Bairro: </label>
      <input type="text" style="width:300px" name="bairro" value="" class="form-control" placeholder="Nome do bairro" id="bairro" required name="bairro">
    </div>
    <div class="form-group">
      <label for="complemento">Complemento: </label>
      <input type="text" style="width:300px" name="complemento" value="" class="form-control" placeholder="Ex: casa, apartamento" id="complemento" required name="complemento">
    </div>
    <div class="form-group">
      <label for="cidade">Cidade: </label>
      <input type="text" style="width:300px" name="cidade" value="" class="form-control" placeholder="Nome da cidade." id="cidade" required name="cidade">
    </div>
    <div class="form-group">
      <label for="estado">UF: </label>
      <select style="width:150px" name="estado" id="estado" class="selectpicker show-tick form-control">
      	<option value="">Selecione</option>
      	<option value="AC">AC</option>
      	<option value="AL">AL</option>
      	<option value="AM">AM</option>
      	<option value="AP">AP</option>
      	<option value="BA">BA</option>
      	<option value="CE">CE</option>
      	<option value="DF">DF</option>
      	<option value="ES">ES</option>
      	<option value="GO">GO</option>
      	<option value="MA">MA</option>
      	<option value="MG">MG</option>
      	<option value="MS">MS</option>
      	<option value="MT">MT</option>
      	<option value="PA">PA</option>
      	<option value="PB">PB</option>
      	<option value="PE">PE</option>
      	<option value="PI">PI</option>
      	<option value="PR">PR</option>
      	<option value="RJ">RJ</option>
      	<option value="RN">RN</option>
      	<option value="RS">RS</option>
      	<option value="RO">RO</option>
      	<option value="RR">RR</option>
      	<option value="SC">SC</option>
      	<option value="SE">SE</option>
      	<option value="SP">SP</option>
      	<option value="TO">TO</option>
      </select>
    </div>
    <div class="form-group">
      <label for="pais">País: </label>
      <input type="text" style="width:150px" name="pais" value="" class="form-control" placeholder="Nome do país." id="pais" required name="pais">
    </div>
    <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
    <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
    <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
  </form>
@stop

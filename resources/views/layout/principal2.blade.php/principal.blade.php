﻿
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PSS Digital</title>
    <link rel="stylesheet" text="text/css" href="/css/app.css">

    <link rel="stylesheet" text="text/css" href="{!! asset('css/sb-admin-2.css') !!}">

    @yield('titulo')
  </head>
  <body>
    <div class="container">
      <div class="page-header">
        <h1 class="text-center">PSS Digital</h1>
      </div>

      @yield('conteudo')
      <footer class="footer">
        <p>PSS Digital 2018 - Processo de Seleção de Professores</p>
      </footer>
    </div>

   <script src="{!! asset('js/jquery.js') !!}" type="text/javascript"></script>
   <script src="{!! asset('js/metisMenu.js') !!}" type="text/javascript"></script>

   <script src="{!! asset('js/sb-admin-2.js') !!}" type="text/javascript"></script>

  </body>
</html>

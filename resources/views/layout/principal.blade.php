﻿
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PSS Digital</title>
    <!-- Bootstrap Core CSS -->
    <link href="{!! asset('css/bootstrap.css') !!}" rel="stylesheet">
    <!-- jquery -->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <!-- MetisMenu CSS -->
    <link href="{!! asset('css/metisMenu.css') !!}" rel="stylesheet">

    <link rel="stylesheet" text="text/css" href="{!! asset('css/sb-admin-2.css') !!}">

    <!-- Custom Fonts -->
    <link href="{!! asset('css/font-awesome.min.css') !!}" rel="stylesheet" type="text/css">

    @yield('titulo')
  </head>
  <body>


        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">PSS Digital</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">

                    <!-- /.dropdown -->

                        <ul class="dropdown-menu dropdown-tasks">

                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">

                        <ul class="dropdown-menu dropdown-alerts">

                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="login.html"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li class="sidebar-search">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                                </div>
                                <!-- /input-group -->
                            </li>
                            <li>
                                <a href="{{ url('/nucleo/form')}}"><i class="fa fa-dashboard fa-fw"></i> Núcleos</a>
                            </li>
                            <li>
                                <a href="dashboard.html"><i class="fa fa-bar-chart-o fa-fw"></i> Escola<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ url('/escola/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Escola</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/turma/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Turma</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/disciplina/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Disciplina</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/cargahoraria/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Carga Horária</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="dashboard.html"><i class="fa fa-bar-chart-o fa-fw"></i> Funcionários<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ url('/funcionario/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Funcionário</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('funcionario/listar')}}"><i class="fa fa-edit fa-fw"></i> Listar Funcionário</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="dashboard.html"><i class="fa fa-bar-chart-o fa-fw"></i> Professores<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ url('/professor/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Professores</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/professor/listar')}}"><i class="fa fa-edit fa-fw"></i> Listar Professores</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="dashboard.html"><i class="fa fa-bar-chart-o fa-fw"></i> Inscrição<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ url('/inscricao/selectprofessor')}}"><i class="fa fa-edit fa-fw"></i> Realizar Inscrição</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/inscricao/listarInscricoes')}}"><i class="fa fa-edit fa-fw"></i> Listar Inscrições</a>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                <a href="dashboard.html"><i class="fa fa-bar-chart-o fa-fw"></i> Documentos<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{ url('/documento_imagem/form')}}"><i class="fa fa-edit fa-fw"></i> Cadastrar Documentos</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('validardocumentos/form')}}"><i class="fa fa-edit fa-fw"></i> Validar Documentos</a>
                                    </li>
                                </ul>
                            </li>


                                <!-- /.nav-second-level -->

                                <!-- /.nav-second-level -->
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">@yield('titulo') </h1>
                    </div>
                    <!-- /.col-lg-12 -->

                    @yield('conteudo')
                </div>
                <!-- /.row -->
            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->


   <script src="{!! asset('js/jquery.js') !!}" type="text/javascript"></script>
   <!-- Bootstrap Core JavaScript -->
   <script src="{!! asset('js/bootstrap.js') !!}"></script>

   <script src="{!! asset('js/metisMenu.js') !!}" type="text/javascript"></script>
   <script src="{!! asset('js/sb-admin-2.js') !!}" type="text/javascript"></script>

  </body>
</html>

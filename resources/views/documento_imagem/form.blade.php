@extends('layout/principal')
@section('conteudo')
			<h1>Cadastrar Documentos</h1>
      <form action="/documento_imagem/create" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if(session('erro'))
            <div class="alert alert-danger">
              {{  session('erro') }}
            </div>
        @endif
				@if(session('sucesso'))
            <div class="alert alert-success">
              {{  session('sucesso') }}
            </div>
        @endif
      <div class="form-group">
				<label for="id_professor">Professor</label>
				<select style="width:300px" name="id_professor" id="id_professor" class="selectpicker show-tick form-control" required name="id_professor">
					<option value="">Selecione um Professor</option>
					@foreach ($professores as $kProfessor => $vProfessor)
		        	<option value="{{$vProfessor->id}}">{{$vProfessor->nome}}</option>
					@endforeach
		    </select>
			</div>
				<div class="form-group">
					<label for="id_ano_inscricao">Ano Inscrição</label>
					<select style="width:300px" name="id_ano_inscricao" id="id_ano_inscricao" class="selectpicker show-tick form-control" required name="id_ano_inscricao">
						<option value="">Selecione um Ano</option>
						@foreach ($ano as $kAno => $vAno)
			        	<option value="{{$vAno->id}}">{{$vAno->ano}}</option>
						@endforeach
			    </select>
      </div>
        <h3>Documentos:</h3>
				@foreach ($documentos as $kDocumento => $vDocumento)
        <div class="form-group">
          <label for="{{$vDocumento->nome}}">{{$vDocumento->titulo}}: </label>
          <input type="file" class="form-control-file" name="{{$vDocumento->nome}}" id="{{$vDocumento->nome}}" pattern="/\.(jpe?g|pdf)$/"/>
				</div>
				@endforeach
        <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
        <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
        <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
      </form>
@stop

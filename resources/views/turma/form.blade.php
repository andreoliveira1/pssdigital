@extends('layout.principal')
@section('titulo')

@stop
@section('conteudo')
  <h1>Cadastrar Turma</h1>
    <form class="" action="/turma/create" method="post">
        {{ csrf_field() }}
        @if(session('erro'))
            <div class="alert alert-danger">
              {{  session('erro') }}
            </div>
        @endif
				@if(session('sucesso'))
            <div class="alert alert-success">
              {{  session('sucesso') }}
            </div>
        @endif
    <div class="form-group">
    <label for="nome">Nome: </label>
    <input style="width:200px" type="text" name="nome" class="form-control" placeholder="Inserir o nome da Turma" id="nome" required name="nome">
    </div>
    <div class="form-group">
      <label for="turno">Turno: </label>
      <select style="width:200px" name="turno" id="turno" class="selectpicker show-tick form-control" required name="turno">
        <option value="">Escolha um Turno</option>
        <option value="manha">Manhã</option>
        <option value="tarde">Tarde</option>
        <option value="noite">Noite</option>
        <option value="integral">Integral</option>
      </select>
    </div>
    <div class="form-group">
      <label for="id_escola">Escola: </label>
      <select style="width:200px" name="id_escola" id="id_escola" class="selectpicker show-tick form-control" required name="id_escola">
        <option value="">Escolha uma Escola</option>
        @foreach ($escolas as $kEscola => $vEscola)
          <option value="{{$vEscola->id}}">{{$vEscola->nome}}</option>
        @endforeach
      </select>
    </div>
      <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
      <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
      <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
    </form>
@stop

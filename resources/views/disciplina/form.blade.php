@extends('layout.principal')
@section('titulo')

@stop
@section('conteudo')
<h1>Cadastrar Disciplina</h1>
  <form class="" action="/disciplina/create" method="post">
      {{ csrf_field() }}
      @if(session('erro'))
          <div class="alert alert-danger">
            {{  session('erro') }}
          </div>
      @endif
      @if(session('sucesso'))
          <div class="alert alert-success">
            {{  session('sucesso') }}
          </div>
      @endif
  <div class="form-group">
      <label for="nome">Nome: </label>
      <input style="width:300px" type="text" class="form-control" name="nome" value="" placeholder="Inserir o nome da disciplina" id="nome" required name="nome">
    </div>
    <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
    <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
    <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
  </form>
@stop

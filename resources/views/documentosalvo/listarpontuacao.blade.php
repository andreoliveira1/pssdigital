@extends('layout/principal')

@section('conteudo')
    <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome Professor</th>
          <th scope="col">Pontuação</th>
          <th scope="col">Ano da Inscricao</th>
        </tr>
      <tbody>
          @foreach($docs as $p)
          <tr>
            <th scope="row">{{$p->id}}</th>
            <td>{{ $p->id_professor }}</td>
            <td>{{ $p->ponto }}</td>
            <td>{{ $p->id_ano_inscricao }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
@stop

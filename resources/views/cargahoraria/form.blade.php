@extends('layout.principal')
@section('titulo')

@stop
@section('conteudo')
  <h1>Cadastrar Carga Horária</h1>
    <form class="" action="/cargahoraria/create" method="post">
        {{ csrf_field() }}
        @if(session('erro'))
            <div class="alert alert-danger">
              {{  session('erro') }}
            </div>
        @endif
				@if(session('sucesso'))
            <div class="alert alert-success">
              {{  session('sucesso') }}
            </div>
        @endif
    <div class="form-group">
    <label for="carga_horaria">Carga Horária: </label>
    <input style="width:200px" type="number" name="carga_horaria" class="form-control" placeholder="Inserir o carga_horaria da Turma" id="carga_horaria" required name="carga_horaria">
    </div>
    <div class="form-group">
      <label for="id_turma">Turma: </label>
      <select style="width:200px" name="id_turma" id="id_turma" class="selectpicker show-tick form-control" required name="id_turma">
        <option value="">Escolha uma Turma</option>
        @foreach ($turmas as $kTurma => $vTurma)
          <option value="{{$vTurma->id}}">{{$vTurma->nome}}</option>
        @endforeach
      </select>
    </div>
    <div class="form-group">
      <label for="id_disciplina">Disciplina: </label>
      <select style="width:200px" name="id_disciplina" id="id_disciplina" class="selectpicker show-tick form-control" required name="id_disciplina">
        <option value="">Escolha uma Disciplina</option>
        @foreach ($disciplinas as $kDisciplina => $vDisciplina)
          <option value="{{$vDisciplina->id}}">{{$vDisciplina->nome}}</option>
        @endforeach
      </select>
    </div>
      <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
      <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
      <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
    </form>
@stop

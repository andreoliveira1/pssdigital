@extends('layout.principal')
@section('titulo')

@stop
@section('conteudo')
  <h1>Distribuir Aulas</h1>
        {{ csrf_field() }}
        @if(session('erro'))
            <div class="alert alert-danger">
              {{  session('erro') }}
            </div>
        @endif
				@if(session('sucesso'))
            <div class="alert alert-success">
              {{  session('sucesso') }}
            </div>
        @endif
        <table class="table table-striped">
            <tr>
              <th scope="col">id</th>
              <th scope="col">Pontução</th>
              <th scope="col">Nome</th>
              <th scope="col">Ação</th>
            </tr>
          <tbody>
              @foreach($profDocValido as $profDoc)
              <tr>
                <td>{{ $profDoc['idProfessor'] }}</td>
                <td>{{ $profDoc['pontuacaoTotal'] }}</td>
                <td>{{ $profDoc['nome'] }}</td>
                <td>
                  <form action="/cargahoraria/distribuiraula" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="idProfessor" value="{{ $profDoc['idProfessor'] }}">
                    <button class="btn btn-primary">0</button>
                  </form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
@stop

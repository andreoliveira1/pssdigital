@extends('layout/principal')

@section('conteudo')
    <h2>Carga Horária Total</h2>
    <table class="table table-striped">
        <tr>
          <th scope="col">Carga Horária (horas)</th>
          <th scope="col">Disciplina</th>
          <th scope="col">Turma</th>
          <th scope="col">Escola</th>
          <th scope="col">Turno</th>
        </tr>
      <tbody>
          @foreach($cargahoraria as $ch)
          <tr>
            <td>{{ $ch->carga_horaria }}</td>
            <td>{{ $ch->nome_disciplina }}</td>
            <td>{{ $ch->nome_turma }}</td>
            <td>{{ $ch->nome_escola }}</td>
            <td>{{ $ch->turno }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
@stop

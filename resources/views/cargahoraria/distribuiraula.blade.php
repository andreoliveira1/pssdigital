@extends('layout.principal')
@section('titulo')

@stop
@section('conteudo')
  <h1>Distribuir Aulas</h1>
        {{ csrf_field() }}
        @if(session('erro'))
            <div class="alert alert-danger">
              {{  session('erro') }}
            </div>
        @endif
				@if(session('sucesso'))
            <div class="alert alert-success">
              {{  session('sucesso') }}
            </div>
        @endif
        <table class="table table-striped">
            <tr>
              <th scope="col">nome</th>
              <th scope="col">disciplina</th>
              <th scope="col">escolas</th>
              <th scope="col">Ação</th>
            </tr>
          <tbody>
            @foreach ($dadoInscricaoProfessor as $dadoIProf)
              <tr>
                <td>{{ $dadoIProf->nome_professor}}</td>
                <td>{{ $dadoIProf->nome_disciplina }}</td>
                <td>{{ $dadoIProf->nome_escola }}</td>
                <td>
                  <form action="/cargahoraria/distribuiraula" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="idProfessor" value="{{ $dadoIProf->id_professor }}">
                    <input type="hidden" name="idProfessor" value="{{ $dadoIProf->id_disciplina }}">
                    <input type="hidden" name="idProfessor" value="{{ $dadoIProf->id_escola }}">
                    <button class="btn btn-primary">0</button>
                  </form>
                </td>
              </tr>
              @endforeach
          </tbody>
        </table>
@stop

@extends('layout.principal')
@section('titulo')
    <title>Validar Documentos</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://sorgalla.com/jcarousel/dist/jquery.jcarousel.min.js?raw=1"></script>
    <script type="text/javascript" src="JScript.js" defer="defer"></script>
@stop
@section('conteudo')
<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="false">
  <ol class="carousel-indicators">
    @foreach ($urlImagem as $kUrlImagem => $pUrlImagem)
      @if($kUrlImagem < 1)
        <li data-target="#myCarousel" data-slide-to="{{$kUrlImagem}}" class="active"></li>
      @endif
      <li data-target="#myCarousel" data-slide-to="{{$kUrlImagem}}" ></li>
    @endforeach
  </ol>
  <div class="carousel-inner" role="listbox" >
    @foreach ($urlImagem as $kUrlImagem => $pUrlImagem)
      @if($kUrlImagem < 1)
        <div class="item active">
          <img src="{{$pUrlImagem}}" alt="Sem Imagem">
        </div>
      @endif
      <div class="item">
        <img src="{{$pUrlImagem}}" alt="Sem Imagem">
      </div>
    @endforeach
  </div>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<div id="validador"></div>
<div class="num"></div>
@stop

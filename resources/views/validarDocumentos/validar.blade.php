@extends('layout.principal')
@section('titulo')
    <title>Validar Documentos</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://sorgalla.com/jcarousel/dist/jquery.jcarousel.min.js?raw=1"></script>
@stop
@section('conteudo')
  <div class="jcarousel-wrapper">
    <div class="jcarousel">
      <ul>
        @foreach ($files as $pFiles)
          <li><img src="{{$pFiles}}" alt=""></img></li>
        @endforeach
      </ul>
    </div>
  </div>
@stop

@extends('layout.principal')
@section('titulo')
  <title>Validar Documentos</title>
@stop
@section('conteudo')
  <form action="/validardocumentos/check" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    @if(session('erro'))
        <div class="alert alert-danger">
          {{  session('erro') }}
        </div>
    @endif
    @if(session('sucesso'))
        <div class="alert alert-success">
          {{  session('sucesso') }}
        </div>
    @endif
    <label for="id_professor">Selecione o professor:</label>
    <div class="form-group">
    <select style="width:300px" name="id_professor" id="id_professor" class="selectpicker show-tick form-control" required name="id_professor">
      <option value="">Escolha um Professor</option>
        @foreach ($professor as $kProfessores => $vProfessores)
            <option value="{{$vProfessores->id}}">{{$vProfessores->nome}}</option>
        @endforeach
    </select>
  </div>
    <input class="btn btn-primary" type="submit" value="Selecionar"></input>
  </form>
@stop

@extends('layout/principal')
@section('conteudo')
    <h1>Configurar Início do Ano Letivo</h1>
    <form class="" action="" method="post">
      {{ csrf_field() }}

      <button class="btn btn-primary" type="nucleo">Cadastrar Núcleo</button>
      <button class="btn btn-primary" type="escola">Cadastrar Escola</button>
      <button class="btn btn-primary" type="diretor/documentador">Cadastrar Diretor/Documentador</button>

      <button class="btn btn-primary" type="submit">Concluir</button>
      <button class="btn btn-primary" type="submit">Voltar</button>

      @stop

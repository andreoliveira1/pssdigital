@extends('layout/principal')

@section('conteudo')
    <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col">Rg</th>
          <th scope="col">Cidade</th>
          <th scope="col">Pais</th>
        </tr>
      <tbody>
          @foreach($professores as $p)
          <tr>
            <th scope="row">{{$p->id}}</th>
            <td>{{ $p->nome }}</td>
            <td>{{ $p->rg }}</td>
            <td>{{ $p->cidade }}</td>
            <td>{{ $p->pais }}</td>
            <td>
              <a href="/professor/mostra?id={{$p->id}}"><span class="glyphicon glyphicon-search"></span></a>
            </td>
            <td>
              <a href="/professor/mostra/{{$p->id}}"><span class="oi-magnifying-glass">Editar</span></a>
            </td>
            <td>
              <a href="/professor/remove/{{$p->id}}"><span class="oi-magnifying-trash">Deletar</span></a>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
@stop

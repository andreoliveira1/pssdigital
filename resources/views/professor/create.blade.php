@extends('layout/principal')
@section('conteudo')
    <h1>Cadastrar Professor</h1>
    <form class="" action="/professor/create" method="post">
      {{ csrf_field() }}
      @if(session('erro'))
          <div class="alert alert-danger">
            {{  session('erro') }}
          </div>
      @endif
      @if(session('sucesso'))
          <div class="alert alert-success">
            {{  session('sucesso') }}
          </div>
      @endif
      <div class="form-group">
        <label for="nome">Nome: </label>
        <input style="width:600px" type="text" name="nome" value="" class="form-control" placeholder="Nome completo" id="nome" required name="nome">
      </div>
      <div class="form-group">
        <label for="cpf">CPF: </label>
        <input style="width:200px" type="number" name="cpf" value="" class="form-control" id="cpf" required name="cpf">
      </div>
      <div class="form-group">
        <label for="rg">RG: </label>
        <input style="width:200px" type="number" name="rg" value="" class="form-control" id="rg" required name="rg">
      </div>
      <div class="form-group">
        <label for="data_nasc">Data de Nascimento: </label>
        <input style="width:200px" type="date" name="data_nasc" value="" class="form-control" id="data_nasc" required name="data_nasc">
      </div>
      <div class="form-group">
        <label for=" telefone">Telefone: </label>
        <input style="width:200px" type="number" name="telefone" value="" class="form-control" id="telefone" required name="telefone">
      </div>
      <div class="form-group">
        <label for="num_dep">Número de Dependentes:</label>
        <input style="width:100px" class="form-control" type="number" name="num_dep">
      </div>
      <div class="form-group">
        <label for="agencia">Agência:</label>
        <input style="width:100px" class="form-control "type="number" name="agencia" required name="agencia">
      </div>
      <div class="form-group">
        <label for="conta_corrente">Conta Corrente:</label>
        <input style="width:100px" class="form-control "type="number" name="conta_corrente" required name="conta_corrente">
      </div>
      <div class="form-group">
        <label for="usuario">Usuário: </label>
        <input style="width:200px" type="text" name="usuario" value="" class="form-control" placeholder="escolha um nome de usuário" id="usuario" required name="usuario">
      </div>
      <div class="form-group">
        <label>Digite um e-mail e uma senha, ele serão seu login:</label>
      </div>
      <div class="form-group">
        <label for="email">Email: </label>
        <input style="width:200px" type="email" name="email" value="" class="form-control" placeholder="ex: joaosilva@hotmail.com" id="email" required name="email">
      </div>
      <div class="form-group">
        <label for="senha">Senha: </label>
        <input style="width:200px" type="password" name="senha" value="" class="form-control" id="senha" required name="senha">
      </div>
      <label>Endereço:</label>
      <div class="form-group">
        <label for="cep">CEP: </label>
        <input style="width:120px" type="number" name="cep" value="" class="form-control" placeholder="00000-000" id="cep" required name="cep">
      </div>
      <div class="form-group">
        <label for="rua">Rua: </label>
        <input style="width:400px" type="text" name="rua" value="" class="form-control" placeholder="Nome da rua" id="rua" required name="rua">
      </div>
      <div class="form-group">
        <label for="numero">Numero: </label>
        <input style="width:120px" type="number" name="numero" value="" class="form-control" placeholder="00000" id="numero">
      </div>
      <div class="form-group">
        <label for="bairro">Bairro: </label>
        <input style="width:200px" type="text" name="bairro" value="" class="form-control" placeholder="Nome do bairro" id="bairro" required name="bairro">
      </div>
      <div class="form-group">
        <label for="complemento">Complemento: </label>
        <input style="width:200px" type="text" name="complemento" value="" class="form-control" placeholder="Ex: casa, apartamento" id="complemento">
      </div>
      <div class="form-group">
        <label for="cidade">Cidade: </label>
        <input style="width:200px" type="text" name="cidade" value="" class="form-control" placeholder="Nome da cidade" id="cidade" required name="cidade">
      </div>
      <div class="form-group">
        <label for="estado">UF: </label>
        <select style="width:200px" name="estado" id="estado" class="form-control" class="selectpicker show-tick form-control" required name="estado">
        	<option value="">Escolha um Estado</option>
        	<option value="AC">AC</option>
        	<option value="AL">AL</option>
        	<option value="AM">AM</option>
        	<option value="AP">AP</option>
        	<option value="BA">BA</option>
        	<option value="CE">CE</option>
        	<option value="DF">DF</option>
        	<option value="ES">ES</option>
        	<option value="GO">GO</option>
        	<option value="MA">MA</option>
        	<option value="MG">MG</option>
        	<option value="MS">MS</option>
        	<option value="MT">MT</option>
        	<option value="PA">PA</option>
        	<option value="PB">PB</option>
        	<option value="PE">PE</option>
        	<option value="PI">PI</option>
        	<option value="PR">PR</option>
        	<option value="RJ">RJ</option>
        	<option value="RN">RN</option>
        	<option value="RS">RS</option>
        	<option value="RO">RO</option>
        	<option value="RR">RR</option>
        	<option value="SC">SC</option>
        	<option value="SE">SE</option>
        	<option value="SP">SP</option>
        	<option value="TO">TO</option>
        </select>
      </div>
      <div class="form-group">
        <label for="pais">País: </label>
        <input style="width:120px" type="text" name="pais" value="" class="form-control" placeholder="Nome do país" id="pais" required name="pais">
      </div>
      <button class="btn btn-primary" type="submit">Cadastrar</button>
      <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
      <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
      </form>

@stop

@extends('layout.principal')
@section('titulo')
    <title>Cadastrar Núcleo</title>
@stop
@section('conteudo')
  <h1>Cadastrar Núcleo</h1>
  <form action="/nucleo/create" method="post">
    <input type="hidden" name="_token" value=" {{ csrf_token() }} ">
    @if(session('erro'))
        <div class="alert alert-danger">
          {{  session('erro') }}
        </div>
    @endif
    @if(session('sucesso'))
        <div class="alert alert-success">
          {{  session('sucesso') }}
        </div>
    @endif
    <div class="form-group">
    <label for="nome">Nome do Setor: </label>
    <input style="width:180px" type="text" name="nome" class="form-control" value="" placeholder="Inserir nome do núcleo" id="nome" required name="nome">
    </div>
    <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
    <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
    <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
  </form>
@stop

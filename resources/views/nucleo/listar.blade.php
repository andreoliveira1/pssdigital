
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <meta charset="utf-8">
    <title>Listar Nucleo</title>
  </head>
  <body>
    <h3>Listagem de Núcleo</h3>
    <table class="table">
      <thead>
        <tr>
          <th>id</th>
          <th>Nome Núcleo</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($nucleos as $kNucleo => $vNucleo)
          <tr>
              <td>{{$kNucleo}}</td>
            <td>{{$vNucleo->setor}}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </body>
</html>

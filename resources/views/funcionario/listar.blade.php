@extends('layout/principal')
@section('conteudo')
    <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col">Rg</th>
          <th scope="col">Cargo</th>
          <th scope="col">Cidade</th>
          <th scope="col">Estado</th>
        </tr>
      <tbody>
      @foreach ($diretores as $diretor)
      <tr>
        <th scope="row">{{$diretor->id}}</th>
        <td>{{$diretor->nome}} </td>
        <td>{{$diretor->rg}} </td>
        <td>{{$diretor->cargo}} </td>
        <td>{{$diretor->cidade}} </td>
        <td>{{$diretor->estado}} </td>
        <td>
          <a href="/Pessoa/mostra?id={{$diretor->id}}"><span class="glyphicon glyphicon-search"></span></a>
        </td>
        <td>
          <a href="/Pessoa/mostra/{{$diretor->id}}"><span class="oi-magnifying-glass">Editar</span></a>
        </td>
        <td>
          <a href="/Pessoa/remove/{{$diretor->id}}"><span class="oi-magnifying-trash">Deletar</span></a>
        </td>
      </tr>
      @endforeach
      @foreach ($documentadores as $documentador)
      <tr>
        <th scope="row">{{$documentador->id}}</th>
        <td>{{$documentador->nome}} </td>
        <td>{{$documentador->rg}} </td>
        <td>{{$documentador->cargo}} </td>
        <td>{{$documentador->cidade}} </td>
        <td>{{$documentador->estado}} </td>
        <td>
          <a href="/Pessoa/mostra?id={{$documentador->id}}"><span class="glyphicon glyphicon-search"></span></a>
        </td>
        <td>
          <a href="/Pessoa/mostra/{{$documentador->id}}"><span class="oi-magnifying-glass">Editar</span></a>
        </td>
        <td>
          <a href="/Pessoa/remove/{{$documentador->id}}"><span class="oi-magnifying-trash">Deletar</span></a>
        </td>
      </tr>
      @endforeach
    </tbody>
    </table>
@stop

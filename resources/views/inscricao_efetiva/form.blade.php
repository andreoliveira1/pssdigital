@extends('layout/principal')

@section('conteudo')
    <h1>Aceitar Inscrição</h1>
    <form class="" action="/inscricao_efetiva/create" method="post">
      {{ csrf_field() }}
      @if(session('erro'))
          <div class="alert alert-danger">
            {{  session('erro') }}
          </div>
      @endif
      @if(session('sucesso'))
          <div class="alert alert-success">
            {{  session('sucesso') }}
          </div>
      @endif
      <div class="form-group" id="inscricoes">
        <label for="id_inscricao">Selecione o Professor:</label>
        <select style="width:300px" name="id_inscricao" id="id_inscricao" class="selectpicker show-tick form-control" required name="id_disciplina">
          <option value="">Escolha uma Inscrição</option>
            @foreach ($inscricoes as $kInscricao => $vInscricao)
                <option value="{{$vInscricao->id}}">
                  {{\App\Http\Controllers\InscricaoEfetivaController::findProfessor($vInscricao->id_professor)}}
                </option>
            @endforeach
        </select>
      </div>
      <div class="form-group" id="detalhes" style="display: none;">
        <label for="disciplina">Disciplina:</label>
        <input type="text" id="disciplina" value="{{\App\Http\Controllers\InscricaoEfetivaController::findDisciplina($vInscricao->id_disciplina)}}" disabled/></br>
        <label for="escola">Escola:</label>
        <input type="text" id="escola" value="{{\App\Http\Controllers\InscricaoEfetivaController::findEscola($vInscricao->id_escola)}}" size="60" disabled/></br>
        <label for="nota">Nota:</label>
        <input type="text" id="nota" value="{{\App\Http\Controllers\InscricaoEfetivaController::findNota($vInscricao->id)}}" size="10" disabled/></br>
        <label for="situacao">Situação:</label>
        <input type="text" id="situacao" value="{{\App\Http\Controllers\InscricaoEfetivaController::findSituacao($vInscricao->id)}}" disabled/>
      </div>
      <script>
        $(document).ready(function(){
          $("#id_inscricao").blur(function(){
            if($("#id_inscricao option:checked").val() === "")
            {
              $("#detalhes").hide("slow");
            }
            else
            {
              $("#detalhes").hide("fast");
              $("#detalhes").show("slow");
            }
          });
        });
      </script>
        <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
        <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
        <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
    </form>
@stop

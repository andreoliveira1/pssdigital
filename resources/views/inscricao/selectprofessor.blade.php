@extends('layout/principal')

@section('conteudo')
    <h1>Realizar Inscrição</h1>
    <form class="" action="/inscricao/principal" method="post">
      {{ csrf_field() }}
      @if(session('erro'))
          <div class="alert alert-danger">
            {{  session('erro') }}
          </div>
      @endif
      @if(session('sucesso'))
          <div class="alert alert-success">
            {{  session('sucesso') }}
          </div>
      @endif
    </div>
    <div class="form-group">
    <label for="id_professor">Selecione o professor:</label>
      <select style="width:300px" name="id_professor" id="id_professor" class="selectpicker show-tick form-control" required name="id_professor">
        <option value="">Escolha um Professor</option>
          @foreach ($professores as $kProfessor => $vProfessor)
              <option value="{{$vProfessor->id}}">{{$vProfessor->nome}}</option>
          @endforeach
      </select>
    </div>
        <button type="submit" name="Selecionar" class="btn btn-primary">Selecionar</button>
      </form>
@stop

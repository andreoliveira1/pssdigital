@extends('layout/principal')

@section('conteudo')
    <table class="table table-striped">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nome</th>
          <th scope="col">Disciplina</th>
          <th scope="col">Escola</th>
          <th scope="col">Ano da Inscricao</th>
        </tr>
      <tbody>
          @foreach($inscricoes as $p)
          <tr>
            <th scope="row">{{$p->id}}</th>
            <td>{{ $p->id_professor }}</td>
            <td>{{ $p->id_disciplina }}</td>
            <td>{{ $p->id_escola }}</td>
            <td>{{ $p->id_ano_inscricao }}</td>
          </tr>
        @endforeach
      </tbody>
    </table>
@stop

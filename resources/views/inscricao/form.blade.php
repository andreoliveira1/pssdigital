@extends('layout/principal')

@section('conteudo')
    <h1>Realizar Inscrição</h1>
    <form class="" action="/inscricao/create" method="post">
      {{ csrf_field() }}
      @if(session('erro'))
          <div class="alert alert-danger">
            {{  session('erro') }}
          </div>
      @endif
      @if(session('sucesso'))
          <div class="alert alert-success">
            {{  session('sucesso') }}
          </div>
      @endif
    </div>
    <div class="form-group">
    <h3>{{$professor->nome}}</h3>
    <input type="hidden" name="id_professor" value="{{$professor->id}}">
    </div>
      <div class="form-group">
        <label for="id_disciplina">Disciplina Pretendida: </label>
        <select style="width:200px" name="id_disciplina" id="id_disciplina" class="selectpicker show-tick form-control" required name="id_disciplina">
          <option value="">Escolha uma Disciplina</option>
          @foreach ($dis as $kDisciplinas => $vDisciplinas)
            <option value="{{$vDisciplinas->id}}">{{$vDisciplinas->nome}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="id_escola">Escola: </label>
        <select style="width:200px" name="id_escola" id="id_escola" class="selectpicker show-tick form-control" required name="id_escola">
          <option value="">Escolha uma Escola</option>
          @foreach ($esc as $kEscolas => $vEscolas)
            <option value="{{$vEscolas->id}}">{{$vEscolas->nome}}</option>
          @endforeach
        </select>
      </div>
      <div class="form-group">
        <label for="id_ano_inscricao">Ano da Inscrição: </label>
        <select style="width:200px" name="id_ano_inscricao" id="id_ano_inscricao" class="selectpicker show-tick form-control" required name="id_ano_inscricao">
          <option value="">Escolha o Ano</option>
          @foreach ($ano as $kAno => $vAno)
            <option value="{{$vAno->id}}">{{$vAno->ano}}</option>
          @endforeach
        </select>
      </div>

        <button type="submit" name="Cadastrar" class="btn btn-primary">Cadastrar</button>
        <button type="button" name="Limpar" class="btn btn-light">Limpar</button>
        <button type="button" name="Cancelar" class="btn btn-danger">Cancelar</button>
      </form>
@stop

@extends('layout/principal')

@section('conteudo')
    <h1>Realizar Inscrição</h1>
      @if(session('erro'))
          <div class="alert alert-danger">
            {{  session('erro') }}
          </div>
      @endif
      @if(session('sucesso'))
          <div class="alert alert-success">
            {{  session('sucesso') }}
          </div>
      @endif
    </div>
    <h3>Olá Professor {{$professor->nome}}!</h3>
    <h3>Poderia realizar a sua inscrição?</h3>

    @if($qtdInscricao < '1')
      @for($i = 0; $i < 3; $i++ )
      <div class="panel panel-primary">
        <div class="panel-heading">
          Inscrição {{($i + 1)}}
        </div>
        <div class="panel-body">
          <form class="" action="/inscricao/form" method="post">
              {{ csrf_field() }}
            <input type="hidden" name="id_professor" id="id_professor" value="{{$professor->id}}">
            <button name="realizar-inscricao" class="btn btn-light">Realizar Inscrição</button>
          </form>
        </div>
      </div>
      @endfor
    @elseif($qtdInscricao > '1')
      @for($i = 0; $i < 3; $i++)
        <div class="panel panel-primary">
          <div class="panel-heading">
            Inscrição {{($i + 1)}}
          </div>
          <div class="panel-body">
            @foreach ($inscricoes as $kInscricao => $vInscricao)

              @if($kInscricao == $i)
              <p>
                Escola: {{$vInscricao->id_escola}}
              </p>
              <p>
                Disciplina: {{$vInscricao->id_disciplina}}
              </p>
              @break
              @elseif($i == 2 && $kInscricao == 2)
              <form class="" action="/inscricao/form" method="post">
                  {{ csrf_field() }}
                <input type="hidden" name="id_professor" id="id_professor" value="{{$professor->id}}">
                <button name="realizar-inscricao" class="btn btn-light">Realizar Inscrição</button>
              </form>
              @endif
            @endforeach
          </div>
        </div>
    @endfor
    @endif
@stop

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnoInscricao extends Model
{
  protected $table = 'ano_inscricao';
  public $timestamps = false;
  protected $fillable = ['id', 'ano'];

}

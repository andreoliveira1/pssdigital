<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscricaoDocumento extends Model
{
  protected $table = 'inscricao_documento';
  public $timestamps = false;
  protected $fillable = ['nota', 'id_ano_inscricao', 'id_professor',
  'id_documentoimagem', 'id_inscricao_um', 'id_inscricao_dois', 'id_inscricao_tres',];

}

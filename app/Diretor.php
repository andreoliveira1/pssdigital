<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diretor extends Model
{
    protected $table = 'diretor';
    public $timestamps = false;
    protected $fillable = array('nome', 'cpf', 'rg', 'data_nasc', 'usuario', 'telefone', 'email', 'senha', 'id_escola');
}

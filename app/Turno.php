<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    protected $table = 'turno';
    public $timestamps = false;
    protected $fillable = ['nome', 'titulo'];
}

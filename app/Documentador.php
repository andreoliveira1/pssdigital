<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documentador extends Model
{
    protected $table = 'documentador';
    public $timestamps = false;
    protected $fillable = array('nome', 'cpf', 'rg', 'data_nasc', 'usuario', 'telefone', 'email', 'senha');
}

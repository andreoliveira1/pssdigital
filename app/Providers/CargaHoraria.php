<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CargaHoraria extends Model
{
    protected $table = 'cargahoraria';
    public $timestamps = false;
    protected $fillable = ['carga_horaria', 'id_disciplina', 'id_turma'];


}

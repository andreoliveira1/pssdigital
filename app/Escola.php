<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Escola extends Model
{
    protected $table = 'escola';
    public $timestamps = false;
    protected $fillable = array('nome', 'id_nucleo', 'id_endereco');
}

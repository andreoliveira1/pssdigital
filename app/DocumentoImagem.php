<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoImagem extends Model
{
  protected $table = 'documentoimagem';
  public $timestamps = false;
  protected $fillable = ['nome', 'titulo'];

}

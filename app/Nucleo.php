<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nucleo extends Model
{
    protected $table = 'nucleo';
    public $timestamps = false;
    //protected $fillable = array('setor');
    protected $fillable = ['nome'];
}

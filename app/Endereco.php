<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table = 'endereco';
    public $timestamps = false;
    protected $fillable = array('cep', 'rua', 'numero', 'bairro', 'complemento', 'cidade', 'estado', 'pais');
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InscricaoEfetiva extends Model
{
  protected $table = 'inscricao_efetiva';
  public $timestamps = false;
  protected $fillable = ['id_inscricao'];
}

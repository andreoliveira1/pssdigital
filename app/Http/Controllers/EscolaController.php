<?php
namespace App\Http\Controllers;
use Request;
use Illuminate\Support\Facades\DB;
use App\Escola;
use App\Endereco;
use App\Nucleo;
use App\Turma;

  class EscolaController extends Controller
  {
    public function form()
    {
      $nucleos = Nucleo::all();
      return view('/escola/form')->with('nucleos', $nucleos);

    }
    public function create()
    {
      $escola = new Escola();
      $endereco = new Endereco();
      $escola->nome = Request::input('nome');
      $escola->id_nucleo = Request::input('nucleo');
      $escola->id_endereco = $endereco->create(Request::except('_token', 'nome', 'nucleo', 'Cadastrar'))->id;
      try {
        $escola->save();
      } catch (\Illuminate\Database\QueryException $e) {
        var_dump($e->errorInfo);
         $enderecoescola = Endereco::find($escola->id_endereco);
         $enderecoescola->delete();
      }
      return redirect('/escola/form')->with('sucesso', 'Escola cadastrada com sucesso.');
    }

    public function addTurma()
    {
      $turma = new Turma();
      $turma->Nome = Request::input('nome');
      //$turma->Turno = Request::input('turno');
      $turma->id_escola = Request::input('escola');
      $turma->add();
      $turma->save();

      //Turma::Create(Request::all());
      //return "Turma criada com Sucesso";
    }

    public function removeTurma()
    {
      $turma = Request::all();
      $turma->delete();
      $turma->save();
    }
  }
?>

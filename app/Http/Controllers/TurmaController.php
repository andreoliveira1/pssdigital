<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Disciplina;
use App\Turno;
use App\Turma;
use App\Escola;

class TurmaController extends Controller
{
  public function form()
  {
    $escolas = Escola::all();
    return view('turma/form')->with("escolas", $escolas);
  }
  public function create(Request $request)
  {
    Turma::Create(Request::all());
    return redirect('/turma/form')->with('sucesso', 'Turma cadastrada com sucesso.');
  }
}

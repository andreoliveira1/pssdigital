<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\DocumentoImagem;
use App\DocumentoSalvo;
use App\Professor;
use App\AnoInscricao;
use App\Repositories\ImageRepository;
use Illuminate\Support\Facades\Storage;


class DocumentoImagemController extends Controller
{
  public function form()
  {
    $documentos = DocumentoImagem::all();
    $professores = Professor::all();
    $ano = AnoInscricao::all();
    return view('/documento_imagem/form')->with('documentos', $documentos)->with('professores', $professores)
                                        ->with('ano', $ano);
  }

  public function create(Request $request)
  {
    $documentoImagem = DocumentoImagem::all();
    foreach ($documentoImagem as $kDocumentoImagem => $vDocumentoImagem) {
      $id_professor = $request->id_professor;
      $id_ano_inscricao = $request->id_ano_inscricao;
      $diretorio = "public/".$id_professor."/".$id_ano_inscricao."/";
      $nomeRequest = $vDocumentoImagem->nome;
      if($vDocumentoImagem->id < 10)
      {
        $nomeArquivo = "0".$vDocumentoImagem->id . "-" . $vDocumentoImagem->nome;
      }
      else
      {
        $nomeArquivo = $vDocumentoImagem->id . "-" . $vDocumentoImagem->nome;
      }
      //
      //O hasFile retorna um boolean dizendo se tem ou não o arquivo.
      if($request->hasFile($nomeRequest))
      {
        //Pega a imagem
        $imagem = $request->file($nomeRequest);
        //Valida a extensão, se retornar um true, é pq a extensão não é aceita. Se retornar false, a extensao está ok.
        if(DocumentoImagemController::validaExtensao($imagem))
        {
          return redirect('/documento_imagem/form')->with('erro', 'Extensao não suportada.');
        }
        //Salva o diretorio  o nome do arquivo, junto com sua extensão original.
        $request->$nomeRequest->storeAS($diretorio, $nomeArquivo.'.'.$imagem->getClientOriginalExtension());
        //Altera o diretório para conter o nome do arquivo.jpeg
        $diretorio = $diretorio.$nomeArquivo.".jpeg";
        // verificar se o arquivo já não foi salvo
        if(DocumentoImagemController::validaDocumentoSalvo($id_professor, $vDocumentoImagem->id, $id_ano_inscricao))
        {
          return redirect('/documento_imagem/form')->with('erro', 'O arquivo duplicado.');
        }
        //validação por hash
        //if(validaHash($imagem))
        //{
        //  return "msg";
        //}
        //finally
        $documentosalvo = new DocumentoSalvo();
        $documentosalvo->caminho = $diretorio;
        $documentosalvo->check = false; //ver se não vai dar erro por no banco agora ser boolean
        $documentosalvo->id_professor = intval($id_professor);
        $documentosalvo->id_ano_inscricao = intval($id_ano_inscricao);
        $documentosalvo->id_documentoimagem = $vDocumentoImagem->id;
        $documentosalvo->save();
      }
      else
     {
       return redirect('/documento_imagem/form')->with('erro', 'Nenhum arquivo selecionado.');
     }
   }
   return redirect('/documento_imagem/form')->with('sucesso', 'Documentos cadastrados com sucesso.');
 }

 public function validaDocumentoSalvo($id_professor, $id_documentoimagem, $id_ano_inscricao)
 {
   if(DB::table('documentosalvo')->where([
                                           ['id_professor', '=', $id_professor],
                                           ['id_documentoimagem', '=', $id_documentoimagem],
                                           ['id_ano_inscricao', '=', $id_ano_inscricao]
                                             ])->exists())
   {
     return true;
   }
   else
   {
     return false;
   }
 }
 //public function validaHash($imagem)
  //{
  //  $hash = md5(readfile($imagem));
    //Falta ser feito
    //Pegar a imagem da onde para comparar? Do diretório? No banco?
  //}
  public function validaExtensao($imagem)
  {
    $regex = '/^(jpe?g|pdf)$/i';
    $extensao = $imagem->getClientOriginalExtension();
    $ola = preg_match($regex, $extensao);
    if(preg_match($regex, $extensao))
    {
      return false;
    }
    else
    {
      return true;
    }
  }

  public function findByIdAll($id)
  {
    $caminho = 'public/'.$id;
    return Storage::allFiles($caminho);
  }
  public function showImagemPorLink($link)
  {
    $url = Storage::url($link);
    return $url;
  }

  public function findById($id)
  {
    $docImg = DocumentoImagem::find($id);
    return $docImg;
  }


}

<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Professor;
use App\DocumentoImagem;
use Illuminate\Support\Facades\Storage;


class ValidarDocumentosController extends Controller
{
    public function form()
    {
      $professores = DB::select('SELECT * FROM professor');
      return view('validardocumentos/form')->with('professor', $professores);
    }

    public function create(Request $request)
    {
      $id = $request->input('professor');
      $files = glob("images/documentos/$id/*.*");
      return view('validarDocumentos/validar')->with('files', $files);
    }

    public function listarDocumentos(Request $request)
    {
      //pegando a opção escolhida na requisição
      $id_professor = $request->input('id_professor');
      //pegando os documentos salvo na tabela e atrelando o id do professor a opção escolhida na requisição
      $documentos = DB::table('documentosalvo')->where('id_professor', $id_professor)->get();
      ######################
      $documentoImagem = new DocumentoImagemController();
      $links = $documentoImagem->findByIdAll($id_professor);
      $urlImagem = [];
      foreach ($links as $link)
      {
        $url = $documentoImagem->showImagemPorLink($link);
        array_push($urlImagem, $url);
      }
      return view('validarDocumentos/check')->with('urlImagem',$urlImagem);
    }
    public function calculaPontuacao()
    {
      $graduação = 0;
      $tempoServiço = 0;
      $aperfeiçoamento = 0;
      switch($i)
      {
        //Nivel Superior Completo
        case 0:
          //Diploma de Licenciatura Plena
          $graduação = 75;
          break;
        case 1:
          //Curso superior completo em qualquer area declaração de matricula e frequencia em curso de licenciatura no minimo 50% da carga horaria
          //Curso superior completo e declaração de matricula e frequencia em curso de formação pedagogica na disciplina de inscrição
          //Curso superior completo e declaração de matricula e frequencia em curso de complementação de licenciatura na disciplina de inscrição
          $graduação = 45;
          break;
        case 2:
          //Curso superior completo em qualquer area declaração de matricula e frequencia em curso de licenciatura no minimo 10% da carga horaria
          $graduação = 30;
          break;
        case 3:
          //Diploma ou certidão de conclusão de curso Bacharelado acompanhado com a mesma nomenclatura da disciplina de inscrição
          $graduação = 25;
          break;
        //Nivel Licenciatura Curta
        case 4:
          //Diploma de Licenciatura Curta na disciplina de inscrição do Ensino Funcamental
          //Diploma de licenciatura plena diferente da disciplina de inscrição
          $graduação = 50;
          break;
        //Nivel Academico
        case 5:
          //Declaração de matricula em curso de licenciatura na disciplina de inscrição, que já tenha cumprido no minimo 50% da carga horaria
          $graduação = 40;
          break;
        case 6:
          //Declaração de matricula em curso de licenciatura na disciplina de inscrição, que já tenha cumprido no minimo 10% da carga horaria
          $graduação = 15;
          break;
        default:
          $graduação = 0;
      }
    }
}

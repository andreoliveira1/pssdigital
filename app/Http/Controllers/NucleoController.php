<?php
namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Nucleo;

  class NucleoController extends Controller
  {
    public function form()
    {
      return view('nucleo/form');
    }
    public function create()
    {
      Nucleo::Create(Request::all());
      return redirect('/nucleo/form')->with('sucesso', 'Nucleo cadastrado com sucesso.');
    }
    
    public function listar()
    {
      $nucleos = Nucleo::all();
      return view('nucleo/listar')->with('nucleos', $nucleos);
    }

    public function emitirEdital()
    {

    }

    public function classificarProfessores()
    {

    }

    public function situacaoRecurso()
    {

    }

    public function carregarInscritos()
    {

    }
  }
?>

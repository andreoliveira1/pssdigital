<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Disciplina;
use App\Inscricao;
use App\InscricaoEfetiva;
use App\Professor;
use App\Turno;
use App\Turma;
use App\Escola;

class InscricaoEfetivaController extends Controller
{
  public function form()
  {
    $inscricoes = Inscricao::all();
    return view('inscricao_efetiva/form')->with("inscricoes", $inscricoes);
  }
  public function create(Request $request)
  {
    InscricaoEfetiva::Create(Request::all());
    return redirect('inscricao_efetiva/form')->with('sucesso', 'Inscrição efetivada com sucesso.');
  }
  public static function findProfessor($id)
  {
    $professor = Professor::find($id)->nome;
    return $professor;
  }
  public static function findDisciplina($id)
  {
    $disciplina = Disciplina::find($id)->nome;
    return $disciplina;
  }
  public static function findEscola($id)
  {
    $escola = Escola::find($id)->nome;
    return $escola;
  }
  public static function findNota($id)
  {
    $nota = Inscricao::find($id)->nota;
    return $nota;
  }
  public static function findSituacao($id)
  {
    $situacao = Inscricao::find($id)->situacao;
    return $situacao;
  }
}

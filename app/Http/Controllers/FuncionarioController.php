<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Request;
use App\Documentador;
use App\Diretor;
use App\Escola;
use App\Endereco;
use App\Cargo;


  class FuncionarioController extends Controller
  {
    public function form()
    {
      $escola = Escola::all();
      return view('funcionario/form')->with('escolas', $escola);
    }

    public function create(Request $request)
    {
      $cargo = Request::input('cargo');
      if($cargo = "documentador")
      {
        $documentador = new Documentador();
        $documentador->nome = Request::input('nome');
        $documentador->cpf = Request::input('cpf');
        $documentador->rg = Request::input('rg');
        $documentador->data_nasc = Request::input('data_nasc');
        $documentador->telefone = Request::input('telefone');
        $documentador->email = Request::input('email');
        $documentador->usuario = Request::input('email');
        $documentador->senha = Request::input('senha');
        $documentador->id_endereco = Endereco::Create(Request::Except('nome', 'cpf', 'rg', 'data_nasc', 'telefone', 'email', 'usuario', 'senha', 'cargo', 'escola'))->id;
        $documentador->save();
      }
      else
      {
        $diretor = new Diretor();
        $diretor->nome = Request::input('nome');
        $diretor->cpf = Request::input('cpf');
        $diretor->rg = Request::input('rg');
        $diretor->data_nasc = Request::input('data_nasc');
        $diretor->telefone = Request::input('telefone');
        $diretor->email = Request::input('email');
        $diretor->usuario = Request::input('email');
        $diretor->senha = Request::input('senha');
        $escola = new Escola();
        $escola = Escola::find(Request::input('escola'));
        $diretor->id_escola = $escola->id;
        $diretor->id_endereco = Endereco::Create(Request::Except('nome', 'cpf', 'rg', 'data_nasc', 'telefone', 'email', 'usuario', 'senha', 'cargo', 'escola'))->id;
        $diretor->save();
      }

      return redirect('/funcionario/form')->with('sucesso', 'Funcionario cadastrado com sucesso.');
    }


    public function listar()
    {
      $diretores = Diretor::all();
      $documentadores = Documentador::all();
      return view('/funcionario/listar')->with('diretores', $diretores)->with('documentadores', $documentadores);
    }
}
?>

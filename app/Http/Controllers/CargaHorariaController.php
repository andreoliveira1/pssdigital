<?php
namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Turma;
use App\CargaHoraria;
use App\Disciplina;

class CargaHorariaController extends Controller
{
  public function form()
  {
    $disciplinas = Disciplina::all();
    $turmas = Turma::all();
    return view('/cargahoraria/form')->with('disciplinas', $disciplinas)->with('turmas', $turmas);
  }
  public function create()
  {
    CargaHoraria::Create(Request::all());
    return redirect('/cargahoraria/form')->with('sucesso', 'Carga Horária cadastrada com sucesso.');
  }

}

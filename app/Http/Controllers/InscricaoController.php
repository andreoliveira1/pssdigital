<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Inscricao;
use App\Escola;
use App\Disciplina;
use App\Professor;
use App\AnoInscricao;
use App\InscricaoDocumento;

class InscricaoController extends Controller
{

  public function principal()
  {
    $request = Request::all();
    $id_professor = $request['id_professor'];
    $id_ano_inscricao = '1';
    $professor = Professor::find($id_professor);
    $resultInscricao = Inscricao::where('id_professor', $id_professor)->where('id_ano_inscricao', $id_ano_inscricao)->get();
    if($resultInscricao->isEmpty()){
      $qtdInscricao = 0;
    } elseif ($resultInscricao->count() < 3){
      $qtdInscricao = 2;
    } else {
      $qtdInscricao = 3;
    }
    return view('inscricao/principal')->with('inscricoes', $resultInscricao)->with('professor', $professor)->with('qtdInscricao', $qtdInscricao);
  }

  public function form()
  {
    $request = Request::all();
    $dis = Disciplina::all();
    $esc = Escola::all();
    $professor = Professor::find($request['id_professor']);
    $ano = AnoInscricao::all();
    return view('inscricao/form')->with('esc', $esc)->with('dis', $dis)->with('professor', $professor)->with('ano', $ano);

  }

  public function create()
  {
    // dados do form que vem pela requisição

    //Auth::user()->cursos()->count();
    //já que é entendido que a contagem de cursos e pelo usuário logado (ou autenticado).
    //Nessa linha Auth::user()->cursos()->count(); é criado 1 SQL para contagem de cursos conforme usuário logado.
    // ---------------------- Usar codigo acima para inscricoes do usuario autenticado ----------

    //regra: 3 disciplinas em 2 escolas

    $request = Request::all();
    $professor = Professor::find($request['id_professor']);
    $id_professor = $professor->id;
    $id_ano_inscricao = $request['id_ano_inscricao'];
    $id_escola = $request['id_escola'];
    $id_disciplina = $request['id_disciplina'];
    $resultInscricao = Inscricao::where('id_professor', $id_professor)->where('id_ano_inscricao', $id_ano_inscricao)->get();
    if ($resultInscricao->isEmpty())
    {
      $inscricao = new Inscricao();
      $inscricao->id_professor = $id_professor;
      $inscricao->id_ano_inscricao = $id_ano_inscricao;
      $inscricao->id_escola = $id_escola;
      $inscricao->id_disciplina = $id_disciplina;
      $inscricao->save();

      $inscricaoDocumento = new InscricaoDocumento();
      $inscricaoDocumento->id_professor = $id_professor;
      $inscricaoDocumento->id_ano_inscricao = $id_ano_inscricao;
      $inscricaoDocumento->id_inscricao_um = $inscricao->id;
      $inscricaoDocumento->save();

      return view('/inscricao/selectprofessor')->with('professores', Professor::all());
    } else {
      if($resultInscricao->count() < 2)
      {
        if(($resultInscricao->where('id_disciplina', $id_disciplina)->where('id_escola', $id_escola))->isEmpty()){
          $inscricao = new Inscricao();
          $inscricao->id_professor = $id_professor;
          $inscricao->id_ano_inscricao = $id_ano_inscricao;
          $inscricao->id_escola = $id_escola;
          $inscricao->id_disciplina = $id_disciplina;
          $inscricao->save();
          InscricaoDocumento::where('id_professor', $id_professor)
                              ->where('id_ano_inscricao', $id_ano_inscricao)
                              ->update(['id_inscricao_dois' => $inscricao->id]);
          return view('/inscricao/selectprofessor')->with('professores', Professor::all());
        }else{
          // mensagem de que não pode fazer a inscrição
          echo "n ok";
          exit();
        }
      }elseif($resultInscricao->count() < 3)
      {
          if(($resultInscricao->where('id_disciplina', $id_disciplina)->where('id_escola', $id_escola))->isEmpty())
          {
              foreach ($resultInscricao as $key => $value)
              {
                //echo $value->id_escola." - ";
                if($value->id_escola == $id_escola)
                {
                  $inscricaoBloqueada = "false";
                  //echo " é igual-posso me inscrever ";
                  $inscricao = new Inscricao();
                  $inscricao->id_professor = $id_professor;
                  $inscricao->id_ano_inscricao = $id_ano_inscricao;
                  $inscricao->id_escola = $id_escola;
                  $inscricao->id_disciplina = $id_disciplina;
                  $inscricao->save();
                  InscricaoDocumento::where('id_professor', $id_professor)
                                    ->where('id_ano_inscricao', $id_ano_inscricao)
                                    ->update(['id_inscricao_tres' => $inscricao->id]);
                  return view('/inscricao/principal')->with('professores', Professor::all());;
                }else{
                  $inscricaoBloqueada = "true";
                  echo " n igual-n posso me inscrever ";
                }
              }
              if($inscricaoBloqueada){
                // manda mensagem diaendo que só pode ser inscrito no máximo em 2 escolas
                echo " mensagem dizendo que só pode ser inscrito no máximo em 2 escolas ";
              }
            //echo "ok";
      }
    }
  }
}


  public function listarInscricoes()
  {
    $inscricoes = Inscricao::all();

    //for (inscricoes){
      //montar array de objetos
    //  $dadosParaView->disciplinaPretendida = $inscricoes->disciplinaPretendida;
    //  $dadosParaView->nomeEscola = select nomeEscola from escolas where idEscola = $inscricoes->escolcas
    //}

    //$dadosParaView = [];
    return view ('inscricao/listarInscricoes')->with('inscricoes', $inscricoes);
  }

  public function listarPorOrdemPontuacao()
  {
    return view ('inscricao/listarPorOrdemPontuacao');
  }
  public function selectProfessor()
  {
    $professores = Professor::all();
    return view('inscricao/selectprofessor')->with('professores', $professores);
  }
}

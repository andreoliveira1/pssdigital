<?php
namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DistribuirAulaController extends Controller{
  public function distribuirAula()
  {

    $idProfessor = Request::all()['idProfessor'];
    $dadoInscricaoProfessor = DB::table('inscricao AS i')
              ->join('professor AS p', 'i.id_professor', '=', 'p.id')
              ->join('disciplina AS d', 'i.id_disciplina', '=', 'd.id')
              ->join('escola AS e', 'i.id_escola', '=', 'e.id')
              ->select('i.id AS id_inscricao',
              'p.id AS id_professor', 'p.nome AS nome_professor', 'p.cpf', 'p.rg', 'p.data_nasc',
              'd.id AS id_disciplina', 'd.nome AS nome_disciplina',
              'e.id AS id_escola', 'e.nome AS nome_escola')
              ->where('p.id', '=', $idProfessor)
              ->get();

    return view('/cargahoraria/distribuiraula')->with('dadoInscricaoProfessor', $dadoInscricaoProfessor);
  }
}

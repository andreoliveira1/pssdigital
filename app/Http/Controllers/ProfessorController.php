<?php

namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Professor;
use App\Endereco;
use App\Inscricao;


class ProfessorController extends Controller{

  public function form()
  {
    return view('/professor/create');
  }

  public function create()
  {
    $professor = new professor();
    $professor->nome = Request::input('nome');
    $professor->cpf = Request::input('cpf');
    $professor->rg = Request::input('rg');
    $professor->data_nasc = Request::input('data_nasc');
    $professor->telefone = Request::input('telefone');
    $professor->email = Request::input('email');
    $professor->usuario = Request::input('email');
    $professor->senha = Request::input('senha');
    $professor->num_dep = Request::input('num_dep');
    $professor->conta_corrente = Request::input('conta_corrente');
    $professor->agencia = Request::input('agencia');
    $professor->id_endereco = Endereco::Create(Request::Except('nome', 'cpf', 'rg', 'data_nasc', 'telefone', 'email', 'usuario', 'senha', 'cargo'))->id;
    $professor->save();

    return redirect('/professor/form')->with('sucesso', 'Professor cadastrado com sucesso.');
  }

  public function remove($id)
  {
    $professor = Professor::find($id);
    $professor->delete();
    return redirect()->action('ProfessorController@listar');
  }

  public function listar()
  {
    $professores = Professor::all();
    return view('/professor/listar')->with('professores', $professores);
  }

  public function visualizarInscricao()
  {
    $inscricao = Professor::find($id);
    return view('inscricao/listarInscricoes');
  }

  public function addInscricao()
  {
    $inscricao = new Inscricao();
    $inscricao->Disciplina_id_disciplina = Request::input('disciplina');
    $inscricao->Escola_id_escola = Request::input('escolas');
    $inscricao->Nota = Reques::input('nota');
    $inscricao->Situacao = Request::input('situacao');
    $inscricao->Professor_id_professor = Request::input('professor');
    $inscricao->add();
    $inscricao->save();
  }

  public function excluirInscricao()
  {
    $inscricao = Professor::find($id);
    $inscricao->delete();
    return redirect()->action('InscricaoController@listarInscricoes');
  }

  public function editarInscricao()
  {
    $inscricao = Professor::find($id);
    $inscricao->update();
    return redirect()->action('InscricaoController@listarInscricoes');
  }

  public function selecionarHorario()
  {

  }
}

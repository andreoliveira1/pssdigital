<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\DocumentoImagem;
use App\Professor;
use App\Repositories\ImageRepository;
use Illuminate\Support\Facades\Storage;


class DocumentoImagemController extends Controller
{
  function form()
  {
    $documentos = DocumentoImagem::all();
    $professores = Professor::all();
    return view('/documento_imagem/form')->with('documentos', $documentos)->with('professores', $professores);
  }

  public function create(Request $request)
  {
    
  }

<?php
namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\DB;
use App\Disciplina;

class DisciplinaController
{
  public function form()
  {
    return view('disciplina/form');
  }
  public function create()
  {
    Disciplina::Create(Request::all());
    return redirect('/disciplina/form')->with('sucesso', 'Disciplina cadastrada com sucesso.');
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

Class Professor extends Model{

  protected $table = 'professor';
  public $timestamps = false;
  protected $fillable = [
    'nome', 'cpf', 'rg', 'data_nasc', 'telefone', 'num_dep',
    'conta_corrente', 'agencia', 'email', 'usuario', 'senha', 'id_endereco'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

Class Inscricao extends Model{

  protected $table = 'inscricao';
  public $timestamps = false;
  protected $fillable = ['id_disciplina', 'id_escola', 'id_professor', 'id_ano_inscricao'];

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentoSalvo extends Model
{
  protected $table = 'documentosalvo';
  public $timestamps = false;
  protected $fillable = ['caminho', 'check', 'id_ano_inscricao', 'id_professor', 'id_documentoimagem'];

}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Documento;

class DocumentoTest extends TestCase
{
    public function testViewDocumento()
    {
      $this->get('documento/create')->assertStatus(200);
      $this->get('documento/create')->assertStatus('documentos');

      //("Não inserir um anexo, sistema deverá informar que o anexo não inserido é obrigatório e deverá
      //ser inserido antes de avançar a próxima etapa");

      //("Inscriçao incompleta, sistema deverá informar msg: alguns dados estão incompletos e/ou faltantes");
      //("Sistema não consegue persistir os dados do professor, sistema deverá informar msg: estamos com problemas
      //no momento, por favor tente novamente em alguns minutos");

    /*public function testPhotoCanBeUploaded()
    
      $this->visit('/upload')
           ->name('File Name', 'name')
           ->attach($absolutePathToFile, 'photo')
           ->press('Upload')
           ->see('Upload Successful!');
    }*/

    }
    public function testDocumentosAnexo()
    {
      \App\Documento::create([

      ]);
    }

    public function testInscricaoCompleta()
    {
      \App\Inscricao::create([
        $disciplinaPretendida => 'disciplinaPretendida',
        $escolas => 'escolas',
        $notaFinal => 'notaFinal',
        $situacao => 'situacao',
      ]);

      $this->seeInDatabase('inscricao' ['disciplinaPretendida', 'escolas', 'notaFinal', 'situacao']);
    }

    public function testDocumentosAfro()
    {
      //if ($inscricao assertTrue('afrodescendente'))
      \App\Documento::create([
        $APPP => 'APPP'
      ]);

      $this->seeInDatabase('documentos' ['APPP']);
    }

    public function testDocumentosDeficiente()
    {
      //if ($inscricao=>assertTrue('deficiencia'))
      //{
      \App\Documento::create([
        $laudoMedico => 'laudoMedico'
      ]);

      $this->seeInDatabase('documentos' ['laudoMedico']);
    }


}

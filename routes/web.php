<?php

Route::get('/nucleo/form', 'NucleoController@form');
Route::post('/nucleo/create', 'NucleoController@create');

Route::get('/escola/form', 'EscolaController@form');
Route::post('/escola/create', 'EscolaController@create');

Route::get('/funcionario/form', 'FuncionarioController@form');
Route::post('/funcionario/create', 'FuncionarioController@create');
Route::get('/funcionario/listar', 'FuncionarioController@listar');

Route::get('/turma/form', 'TurmaController@form');
Route::post('/turma/create', 'TurmaController@create');

Route::get('/disciplina/form', 'DisciplinaController@form');
Route::post('/disciplina/create', 'DisciplinaController@create');

Route::get('/professor/form', 'ProfessorController@form');
Route::post('/professor/create', 'ProfessorController@create');
Route::post('/professor/remove', 'ProfessorController@remove');
Route::get('/professor/listar', 'ProfessorController@listar');

Route::post('/inscricao/form', 'InscricaoController@form');
Route::post('/inscricao/principal', 'InscricaoController@principal');
Route::post('/inscricao/create', 'InscricaoController@create');
Route::get('/inscricao/listarInscricoes', 'InscricaoController@listarInscricoes');
Route::get('/inscricao/listarPorOrdemPontuacao', 'InscricaoController@listarPorOrdemPontuacao');
Route::get('/inscricao/selectprofessor', 'InscricaoController@selectProfessor');

Route::get('/documento_imagem/form', 'DocumentoImagemController@form');
Route::post('/documento_imagem/create', 'DocumentoImagemController@create');
Route::get('/documentos/upload', 'DocumentoImagemController@upload');
Route::post('/documentos/move', 'DocumentoImagemController@move');

Route::get('/validardocumentos/form', 'ValidarDocumentosController@form');
Route::post('/validardocumentos/create', 'ValidarDocumentosController@create');
Route::post('/validardocumentos/check', 'ValidarDocumentosController@listarDocumentos');

Route::get('/cargahoraria/form', 'CargaHorariaController@form');
Route::post('/cargahoraria/create', 'CargaHorariaController@create');

Route::get('/inscricao_efetiva/form', 'InscricaoEfetivaController@form');
Route::post('/inscricao_efetiva/create', 'InscricaoEfetivaController@create');
?>

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nome');
          $table->string('cpf');
          $table->string('rg');
          $table->date('data_nasc');
          $table->string('telefone');
          $table->string('email')->unique();
          $table->string('usuario');
          $table->string('senha');
          $table->enum('cargo', ['Documentador', 'Diretor']);
          $table->unsignedInteger('id_endereco');
          $table->foreign('id_endereco')->references('id')->on('enderecos');
          $table->rememberToken();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInscricoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscricoes', function (Blueprint $table) {
            $table->increments('id');
            $table->nota('nota');
            $table->enum('situacao', ['Espera', 'Recurso', 'Aprovada', 'Reprovada']);
            $table->unsignedInteger('id_escola');
            $table->unsignedInteger('id_disciplina');
            $table->unsignedInteger('id_professor');
            $table->foreign('id_escola')->references('id')->on('escolas');
            $table->foreign('id_disciplina')->references('id')->on('disciplinas');
            $table->foreign('id_professor')->references('id')->on('professores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscricoes');
    }
}

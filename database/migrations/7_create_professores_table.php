<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('cpf');
            $table->string('rg');
            $table->date('data_nasc');
            $table->string('telefone');
            $table->string('email')->unique();
            $table->string('usuario');
            $table->string('senha');
            $table->integer('num_dep');
            $table->integer('conta_corrente');
            $table->integer('agencia');
            $table->unsignedInteger('id_endereco');
            $table->foreign('id_endereco')->references('id')->on('enderecos');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professores');
    }
}

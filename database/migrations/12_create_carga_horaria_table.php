<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class 12CreateCargaHorariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carga_horaria', function (Blueprint $table) {
            $table->unsignedInteger('id_disciplina');
            $table->unsignedInteger('id_turma');
            $table->foreign('id_escola')->references('id')->on('escolas');
            $table->foreign('id_turma')->references('id')->on('turmas');
            $table->integer('carga_horaria');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carga_horaria');
    }
}
